import urllib.request


class HtmlPageDownloader:
    def __init__(self, url):
        self.url = url
        print("url:"+url)
        self.response = None

    def get(self):
        fp = urllib.request.urlopen("http://"+self.url)
        bytes = fp.read()
        dom = bytes.decode("utf8")
        fp.close()
        return dom
