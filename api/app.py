from flask import Flask
from page_title_extractor import PageTitleExtractor
from html_page_downloader import HtmlPageDownloader

app = Flask(__name__)


@app.route('/')
def main():
    return 'API server is up.'


@app.route('/title/<url>')
def extract_title(url: str):
    print("URL given as argument:"+url)
    extractor = PageTitleExtractor(HtmlPageDownloader(url).get())
    return {
        "page_title": extractor.extract().title,
        "greetings": "How many wows"
    }


if __name__ == "__main__":
    app.run(host='0.0.0.0')
