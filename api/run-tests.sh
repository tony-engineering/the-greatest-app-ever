docker build -t the-greatest-app-ever .
docker run -e PYTHONPATH=/app/api --rm the-greatest-app-ever bash -c "python3 -m unittest discover -v -p *_test.py"
